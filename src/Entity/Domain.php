<?php

namespace Nubex\SelectelDNS\Entity;

use Nubex\SelectelDNS\Client;

class Domain {

    private $client;
    private $baseEndpoint;

    /**
     * @param Client $client
     * @param string $baseEndpoint
     */
    public function __construct(Client $client, $baseEndpoint)
    {
        $this->client = $client;
        $this->baseEndpoint = $baseEndpoint;
    }

    /**
     * @return array|mixed
     */
    public function getList()
    {
        return $this->client->get($this->baseEndpoint);
    }

    /**
     * @param string $domainName
     * @param string|null $bindZone
     * @return array|mixed
     */
    public function create($domainName, $bindZone = null)
    {
        $body = [
            'name' => $domainName,
        ];
        if ($bindZone) {
            $body['bind_zone'] = $bindZone;
        }
        return $this->client->post($this->baseEndpoint, $body);
    }

    /**
     * @param int|string $domainIdOrName
     * @return array|mixed
     */
    public function getInfo($domainIdOrName)
    {
        return $this->client->get($this->baseEndpoint . $domainIdOrName);
    }

    /**
     * @param int $domainId
     * @return array|mixed
     */
    public function getZone($domainId)
    {
        return $this->client->get($this->baseEndpoint . $domainId . '/export');
    }

    /**
     * @param int $domainId
     * @param array $tags
     * @return array|mixed
     */
    public function update($domainId, array $tags)
    {
        $body = [
            'tags' => $tags,
        ];
        return $this->client->patch($this->baseEndpoint . $domainId, $body);
    }

    /**
     * @param int $domainId
     * @return array|mixed
     */
    public function delete($domainId)
    {
        return $this->client->delete($this->baseEndpoint . $domainId);
    }

}
