<?php

namespace Nubex\SelectelDNS\Entity;

use Nubex\SelectelDNS\Client;

class Tag {

    private $client;
    private $baseEndpoint;

    /**
     * @param Client $client
     * @param string $baseEndpoint
     */
    public function __construct(Client $client, $baseEndpoint)
    {
        $this->client = $client;
        $this->baseEndpoint = $baseEndpoint;
    }

    /**
     * @return array|mixed
     */
    public function getList()
    {
        return $this->client->get($this->baseEndpoint . 'tags/');
    }

    /**
     * @param string $tagName
     * @param array $domainIds
     * @return array|mixed
     */
    public function create($tagName, $domainIds = [])
    {
        $body = [
            'name' => $tagName,
        ];
        if ($domainIds) {
            $body['domains'] = $domainIds;
        }
        return $this->client->post($this->baseEndpoint . 'tags/', $body);
    }

    /**
     * @param int $tagId
     * @return array|mixed
     */
    public function getInfo($tagId)
    {
        return $this->client->get($this->baseEndpoint . 'tags/' . $tagId);
    }

    /**
     * @param int $tagId
     * @param string $newTagName
     * @param array $newDomainIds
     * @return array|mixed
     */
    public function update($tagId, $newTagName, $newDomainIds = [])
    {
        $body = [
            'name' => $newTagName,
        ];
        if ($newDomainIds) {
            $body['domains'] = $newDomainIds;
        }
        return $this->client->put($this->baseEndpoint . 'tags/' . $tagId, $body);
    }

    /**
     * @param int $tagId
     * @return array|mixed
     */
    public function delete($tagId)
    {
        return $this->client->delete($this->baseEndpoint . 'tags/' . $tagId);
    }

}
