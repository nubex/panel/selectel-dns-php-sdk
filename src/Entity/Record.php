<?php

namespace Nubex\SelectelDNS\Entity;

use Nubex\SelectelDNS\Client;

class Record {

    private $client;
    private $baseEndpoint;

    /**
     * @param Client $client
     * @param string $baseEndpoint
     */
    public function __construct(Client $client, $baseEndpoint)
    {
        $this->client = $client;
        $this->baseEndpoint = $baseEndpoint;
    }

    /**
     * @param int|string $domainIdOrName
     * @return array|mixed
     */
    public function getList($domainIdOrName)
    {
        return $this->client->get($this->baseEndpoint . $domainIdOrName . '/records/');
    }

    /**
     * @param int $domainId
     * @param array $otherParams
     * @return array|mixed
     *
     * otherParams:
     *
     * name required (string)
     * Имя записи
     * type required (string)
     * Тип записи (SOA, NS, A/AAAA, CNAME, SRV, MX, TXT, SPF)
     * location (string)
     * Локация
     * ttl (int)
     * Время жизни
     * email (string)
     * e-mail администратора домена (только у SOA)
     * content (string)
     * Содержимое записи (нет у SRV)
     * priority (int)
     * Приоритет записи (только у MX и SRV)
     * weight (int)
     * Относительный вес для записей с одинаковым приоритетом (только у SRV)
     * port (int)
     * Порт сервиса (только у SRV)
     * target (string)
     * Имя хоста сервиса (только у SRV)
     */
    public function create($domainId, $otherParams = [])
    {
        return $this->client->post($this->baseEndpoint . $domainId . '/records/', $otherParams);
    }

    /**
     * @param int $domainId
     * @param int $recordId
     * @param array $otherParams
     * @return array|mixed
     *
     * Параметры аналогично create
     */
    public function update($domainId, $recordId, array $otherParams = [])
    {
        return $this->client->put($this->baseEndpoint . $domainId . '/records/' . $recordId, $otherParams);
    }

    /**
     * @param int $domainId
     * @param int $recordId
     * @return array|mixed
     */
    public function delete($domainId, $recordId)
    {
        return $this->client->delete($this->baseEndpoint . $domainId . '/records/' . $recordId);
    }

    /**
     * @param $domainName
     * @return array|mixed
     *
     * Не понятно, что именно должна делать эта ф-ция
     */
    public function batchUpdate($domainName)
    {
        return $this->client->get($this->baseEndpoint . $domainName . '/records/batch_update/');
    }

}
