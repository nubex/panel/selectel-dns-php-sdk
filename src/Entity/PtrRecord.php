<?php

namespace Nubex\SelectelDNS\Entity;

use Nubex\SelectelDNS\Client;

class PtrRecord {

    private $client;
    private $baseEndpoint;

    /**
     * @param Client $client
     * @param string $baseEndpoint
     */
    public function __construct(Client $client, $baseEndpoint)
    {
        $this->client = $client;
        $this->baseEndpoint = $baseEndpoint;
    }

    /**
     * @return array|mixed
     */
    public function getList()
    {
        return $this->client->get($this->baseEndpoint . 'ptr/');
    }

    /**
     * @param string $ip
     * @param string $domainName
     * @return array|mixed
     */
    public function create($ip, $domainName)
    {
        $body = [
            'ip' => $ip,
            'content' => $domainName,
        ];
        return $this->client->post($this->baseEndpoint . 'ptr/', $body);
    }

    /**
     * @param int $ptrId
     * @return array|mixed
     */
    public function getInfo($ptrId)
    {
        return $this->client->get($this->baseEndpoint . 'ptr/' . $ptrId);
    }

    /**
     * @param int $ptrId
     * @param string $ip
     * @param string $domainName
     * @return array|mixed
     */
    public function update($ptrId, $ip, $domainName)
    {
        $body = [
            'ip' => $ip,
            'content' => $domainName,
        ];
        return $this->client->put($this->baseEndpoint . 'ptr/' . $ptrId, $body);
    }

    /**
     * @param int $ptrId
     * @return array|mixed
     */
    public function delete($ptrId)
    {
        return $this->client->delete($this->baseEndpoint . 'ptr/' . $ptrId);
    }

}
