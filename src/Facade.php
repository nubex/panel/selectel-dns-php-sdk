<?php

namespace Nubex\SelectelDNS;

use GuzzleHttp\Client as HttpClient;
use Nubex\SelectelDNS\Entity\Domain;
use Nubex\SelectelDNS\Entity\PtrRecord;
use Nubex\SelectelDNS\Entity\Record;
use Nubex\SelectelDNS\Entity\Tag;

class Facade
{
    const BASE_ENDPOINT = 'domains/v1/';

    private $apiKey;
    private $apiUrl;
    private $client = null;
    private $httpClient = null;
    private $httpOptions;
    private $maxRetry;

    /**
     * @param string $apiUrl
     * @param string $apiKey
     * @param array $httpOptions
     * @param int $maxRetry
     */
    public function __construct($apiUrl, $apiKey, $httpOptions = [], $maxRetry = 1)
    {
        $this->apiUrl = $apiUrl;
        $this->apiKey = $apiKey;
        $this->httpOptions = $httpOptions;
        $this->maxRetry = $maxRetry;
    }

    /**
     * @param HttpClient $httpClient
     * @return Facade
     */
    public function setHttpClient(HttpClient $httpClient)
    {
        $this->httpClient = $httpClient;
        return $this;
    }

    /**
     * @return HttpClient
     */
    public function getHttpClient()
    {
        if ($this->httpClient === null) {
            $this->httpClient = new HttpClient();
        }
        return $this->httpClient;
    }

    /**
     * @return Client
     */
    public function getClient()
    {
        if ($this->client === null) {
            $this->client = new Client($this->apiUrl, $this->apiKey, $this->getHttpClient(), $this->maxRetry);
            $this->client->setHttpOptions($this->httpOptions);
            $this->client->setHttpOption('http_errors', false);
        }
        return $this->client;
    }

    /**
     * @return Domain
     */
    public function domains()
    {
        return new Domain($this->getClient(), self::BASE_ENDPOINT);
    }

    /**
     * @return Record
     */
    public function records()
    {
        return new Record($this->getClient(), self::BASE_ENDPOINT);
    }

    public function ptrRecords()
    {
        return new PtrRecord($this->getClient(), self::BASE_ENDPOINT);
    }

    public function tags()
    {
        return new Tag($this->getClient(), self::BASE_ENDPOINT);
    }

}
