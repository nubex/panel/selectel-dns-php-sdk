<?php

use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Exception\RequestException;
use Nubex\SelectelDNS\Client;

class ClientTest extends PHPUnit_Framework_TestCase
{
    private $baseUrl = 'http://localhost/api';
    private $apiKey = '12345678';

    private $client;

    public function testHttpOptions()
    {
        $httpClient = new HttpClient();

        $client = new Client($this->baseUrl, $this->apiKey, $httpClient);

        $this->assertEquals(array(), $client->getHttpOptions(),
            "По умолчанию пустой массив дополнительный опций для guzzle");

        $client->setHttpOptions(['testName' => 'testValue']);
        $this->assertEquals(['testName' => 'testValue'], $client->getHttpOptions(),
            "Содержит единственный тестовый параметр");

        $client->setHttpOption('testName', 'testValue2');
        $this->assertEquals(['testName' => 'testValue2'], $client->getHttpOptions(),
            "Содержит измененнный тестовый параметр");

        $client->setHttpOption('testName2', 'testValue');
        $this->assertEquals(['testName' => 'testValue2', 'testName2' => 'testValue'], $client->getHttpOptions(),
            "Содержит добавленный тестовый параметр");

        $client->setHttpOptions([]);
        $this->assertEquals([], $client->getHttpOptions(), "Содержит пустой массив параметров");
    }

    public function testBadContentTypeForGet()
    {
        $this->doTestBadContentType('get');
    }

    public function testBadContentTypeForPost()
    {
        $this->doTestBadContentType('post');
    }

    public function testBadContentTypeForDelete()
    {
        $this->doTestBadContentType('delete');
    }

    public function testBadContentTypeForPut()
    {
        $this->doTestBadContentType('put');
    }

    protected function doTestBadContentType($method)
    {
        $bodyCorrect = json_encode(['test' => 'value']);

        $mock = new MockHandler([
            new Response(200, [], $bodyCorrect),
        ]);

        $handler = HandlerStack::create($mock);
        $httpClient = new HttpClient(['handler' => $handler]);

        $client = new Client($this->baseUrl, $this->apiKey, $httpClient);

        $this->expectException(\Exception::class);
        $client->$method('endpoint');
    }

    public function testEmptyBodyForGet()
    {
        $this->doTestEmptyBody('get');
    }

    public function testEmptyBodyForPost()
    {
        $this->doTestEmptyBody('post');
    }

    public function testEmptyBodyForPut()
    {
        $this->doTestEmptyBody('put');
    }

    public function testEmptyBodyForDelete()
    {
        $this->doTestEmptyBody('delete');
    }

    protected function doTestEmptyBody($method)
    {
        $bodyEmpty = '';

        $mock = new MockHandler([
            new Response(200, ['content-type' => 'application/json'], $bodyEmpty),
        ]);

        $handler = HandlerStack::create($mock);
        $httpClient = new HttpClient(['handler' => $handler]);

        $client = new Client($this->baseUrl, $this->apiKey, $httpClient);

        $this->expectException(\Exception::class);
        $client->$method('endpoint');
    }


    public function testWrongBodyForGet()
    {
        $this->doTestWrongBody('get');
    }

    public function testWrongBodyForPost()
    {
        $this->doTestWrongBody('post');
    }

    public function testWrongBodyForPut()
    {
        $this->doTestWrongBody('put');
    }

    public function testWrongBodyForDelete()
    {
        $this->doTestWrongBody('delete');
    }

    protected function doTestWrongBody($method)
    {
        $bodyWrong = 'wewtewtweterwr';

        $mock = new MockHandler([
            new Response(200, ['content-type' => 'application/json'], $bodyWrong),
        ]);

        $handler = HandlerStack::create($mock);
        $httpClient = new HttpClient(['handler' => $handler]);

        $client = new Client($this->baseUrl, $this->apiKey, $httpClient);

        $this->expectException(\RuntimeException::class);
        $client->$method('endpoint');
    }

    public function testCorrectBodyForGet()
    {
        $this->doTestCorrectBody('get', 'GET');
    }

    public function testCorrectBodyForPost()
    {
        $this->doTestCorrectBody('post', 'POST');
    }

    public function testCorrectBodyForPut()
    {
        $this->doTestCorrectBody('put', 'PUT');
    }

    public function testCorrectBodyForDelete()
    {
        $this->doTestCorrectBody('delete', 'DELETE');
    }

    protected function doTestCorrectBody($method, $httpMethod)
    {
        $d = ['test' => 'value'];
        $bodyCorrect = json_encode($d);

        $mock = new MockHandler([
            new Response(200, ['content-type' => 'application/json'], $bodyCorrect),
        ]);

        $handler = HandlerStack::create($mock);
        $httpClient = new HttpClient(['handler' => $handler]);

        $client = new Client($this->baseUrl, $this->apiKey, $httpClient);

        $data = $client->$method('endpoint');
        $this->assertEquals($d, $data);

        $lastRequest = $client->getLastRequest();
        $this->assertEquals($this->baseUrl . '/endpoint', (string)$lastRequest->getUri());

        $this->assertEquals($httpMethod, $lastRequest->getMethod());

        $lastResponse = $client->getLastResponse();
        $this->assertEquals(['application/json'], $lastResponse->getHeader('content-type'));
    }

}
