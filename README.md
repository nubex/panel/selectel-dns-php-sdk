
PHP SDK для работы с Selectel DNS API
=================

PHP библиотека для работы с API Selectel DNS


## Установка для разработки

``` sh
git clone git@gitlab.com:nubex/selectel-dns-php-sdk.git
cd selectel-dns-php-sdk
composer install --dev
composer test
```
## Установка для использования

Подключаем в свой composer файл

``` text
composer require nubex/selectel-dns-php-sdk
```

## Использование

``` php
$facade = new \Nubex\SelectelDNS\Facade('https://api.selectel.ru', API_KEY, $httpOptionsArray, 3);
```

### Методы для клиента


